(function(window) {
    //兼容
    window.URL = window.URL || window.webkitURL;
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

    var HZRecorder = function(stream, config) {
        config = config || {};
        config.sampleBits = config.sampleBits || 8; //采样数位 8, 16
        config.sampleRate = config.sampleRate || (44100 / 6); //采样率(1/6 44100)

        var context = new(window.webkitAudioContext || window.AudioContext)();
        var audioInput = context.createMediaStreamSource(stream);
        var createScript = context.createScriptProcessor || context.createJavaScriptNode;
        var recorder = createScript.apply(context, [16384, 1, 1]);
        lame = new lamejs();
        mp3Encoder = new lame.Mp3Encoder(1, config.sampleRate || 44100, config.sampleBits || 128);


        var floatTo16BitPCM = function(input, output) {
            for (var i = 0; i < input.length; i++) {
                var s = Math.max(-1, Math.min(1, input[i]));
                output[i] = (s < 0 ? s * 0x8000 : s * 0x7FFF);
            }
        };

        var convertBuffer = function(arrayBuffer) {
            var data = new Float32Array(arrayBuffer);
            var out = new Int16Array(arrayBuffer.length);
            floatTo16BitPCM(data, out);
            return out;
        };

        var encode = function(arrayBuffer) {
            var transformData = [];
            var maxSamples = 1152;
            var samplesMono = convertBuffer(arrayBuffer);
            var remaining = samplesMono.length;
            for (var i = 0; remaining >= 0; i += maxSamples) {
                var left = samplesMono.subarray(i, i + maxSamples);
                var mp3buf = mp3Encoder.encodeBuffer(left);
                transformData.push(mp3buf);
                remaining -= maxSamples;
            }
            // transformData.push(mp3Encoder.flush());
            return transformData;
        };
        var audioData = {
            size: 0 //录音文件长度
                ,
            buffer: [] //录音缓存
                ,
            callback: config.callback,
            inputSampleRate: context.sampleRate //输入采样率
                ,
            inputSampleBits: 16 //输入采样数位 8, 16
                ,
            outputSampleRate: config.sampleRate //输出采样率
                ,
            oututSampleBits: config.sampleBits //输出采样数位 8, 16
                ,
            input: function(data) {
                //this.buffer = [];
                // this.buffer.push(new Float32Array(data));
                this.buffer = data;
                //this.buffer = new Float32Array(data);
                this.size += data.length;
                this.encodeMP3();

            },
            encodeMP3: function() {
                var that = this;
                var mp3Buffer = encode(this.buffer);
                var blob = new Blob(mp3Buffer, {
                    type: 'audio/mp3'
                });
                var reader = new FileReader();
                reader.readAsArrayBuffer(blob);
                reader.onload = function(e) {
                    var buf = new Uint8Array(reader.result);
                    var dataLength = buf.length;
                    that.callback && that.callback(config.sampleRate, config.sampleBits, dataLength, buf);
                }
            }

        };

        //开始录音
        this.start = function() {
            audioInput.connect(recorder);
            recorder.connect(context.destination);
        }

        //停止
        this.stop = function() {

            recorder.disconnect();
        }

        //音频采集
        recorder.onaudioprocess = function(e) {
            audioData.input(e.inputBuffer.getChannelData(0));
            //record(e.inputBuffer.getChannelData(0));
        }

    };
    //抛出异常
    HZRecorder.throwError = function(message) {
            window.isRecordingRights = false;
            // new Vue().$Message.error(message);
            // throw new function () { this.toString = function () { return message; } }
        }
        //是否支持录音
    HZRecorder.canRecording = (navigator.getUserMedia != null);
    //获取录音机
    HZRecorder.get = function(callback, config) {
        if (callback) {
            if (navigator.getUserMedia) {
                navigator.getUserMedia({
                        audio: true,
                        audio: true
                    } //只启用音频
                    ,
                    function(stream) {
                        var rec = new HZRecorder(stream, config);
                        callback(rec);
                    },
                    function(error) {

                        switch (error.code || error.name) {
                            case 'PERMISSION_DENIED':
                            case 'PermissionDeniedError':
                                HZRecorder.throwError('用户拒绝提供信息。');
                                break;
                            case 'NOT_SUPPORTED_ERROR':
                            case 'NotSupportedError':
                                HZRecorder.throwError('浏览器不支持硬件设备。');
                                break;
                            case 'MANDATORY_UNSATISFIED_ERROR':
                            case 'MandatoryUnsatisfiedError':
                                HZRecorder.throwError('无法发现指定的硬件设备。');
                                break;
                            case 8:
                                HZRecorder.throwError('没有检测到录音设备,无法录音。');
                                break;
                            default:
                                HZRecorder.throwError('无法打开麦克风。异常信息:' + (error.code || error.name));
                                break;
                        }
                    });
            } else {
                isRecordingRights = false
                    // HZRecorder.throwError('当前浏览器不支持录音功能。');
                return;
            }
        }
    }

    // 判断端字节序
    HZRecorder.littleEdian = (function() {
        var buffer = new ArrayBuffer(2);
        new DataView(buffer).setInt16(0, 256, true);
        return new Int16Array(buffer)[0] === 256 ? 1 : 0;
    })();

    window.HZRecorder = HZRecorder;

})(window);