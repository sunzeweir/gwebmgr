var isNeedTalk = document.location.protocol == "https:";
var wsHost = null;
var viewhost = null;
var viewhosts = null;
var apihost = null;
var apihosts = null;
var isWebrtcPlay = false;

var ishttps = 'https:' == document.location.protocol ? true : false;
var lastIndex = location.pathname.lastIndexOf("/v3");
if (lastIndex < 0) {
    lastIndex = location.pathname.lastIndexOf("/");
}
var path = location.pathname.substr(0, lastIndex);

viewhost = location.protocol + "//" + location.host + path + "/";

//var hostDomain = "54.179.186.251";
var hostDomain = "gps51.com";

if (location.hostname.indexOf('localhost') != -1 || location.hostname.indexOf('127.0.0.1') != -1) {
    viewhosts = viewhost;
    if (location.pathname.indexOf("gpsserver") != -1) {
        apihost = viewhost;
        apihosts = viewhosts;
    } else {
        apihost = 'https://www.gps51.com:8443/';
        apihosts = 'https://www.gps51.com:8443/';
    }
    wsHost = "ws://localhost:90";
} else {
    if (isNeedTalk == true) {
        viewhosts = "https://" + location.host + path + "/";
    } else {
        viewhosts = "http://" + location.host + path + "/";
    }
    apihost = "https://" + hostDomain + ":8443/";
    apihosts = "https://" + hostDomain + ":8443/";
    wsHost = "wss://" + hostDomain + "/wss";
}

window.viewhosts = viewhosts;
window.apihosts = apihosts;